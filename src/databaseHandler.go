package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	_ "github.com/go-sql-driver/mysql"
)

// Mendapatkan Semua data jalan yang terdapat dalam database dummy
func GetAllStreet(w http.ResponseWriter, r *http.Request){
	db,err := sql.Open("mysql", "root:@tcp(167.205.67.251:3306)/batifo_db")
	if err!=nil{
		log.Fatal(err)
	}
	defer db.Close()
	

	rows,err := db.Query("select street_id, street_name, street_lat, street_lng, street_time1, street_time2, street_time3, street_time4, street_time5, street_time6, street_time7 from street_data")
	if err!=nil {
		log.Fatal(err)
	}
	defer rows.Close()
	sd := streetData{}
	data := showStreet{}
	print := []showStreet{}
	for rows.Next(){
		err := rows.Scan(&sd.Street_id, &sd.Street_name, &sd.Street_lat, &sd.Street_lng,&sd.Street_time1, &sd.Street_time2, &sd.Street_time3, &sd.Street_time4, &sd.Street_time5, &sd.Street_time6, &sd.Street_time7)	
		if err!=nil{
			log.Fatal(err)
		}
		data.Street_name = sd.Street_name
		data.Street_lat = sd.Street_lat
		data.Street_lng = sd.Street_lng
		print = append (print,data)	
	}
	json.NewEncoder(w).Encode(&print)
	err = rows.Err()
}
// Mendapatkan semua data traffic yang terdapat dalam database dummy
func GetAllTraffic (w http.ResponseWriter, r *http.Request){
	db,err := sql.Open("mysql", "root:@tcp(167.205.67.251:3306)/batifo_db")
        if err!=nil{
                log.Fatal(err)
        }
        defer db.Close()


        rows,err := db.Query("select street_id, street_length, vehicle_volume_1, avg_speed_1, vehicle_volume_2, avg_speed_2, vehicle_volume_3, avg_speed_3 from traffic_data")
        if err!=nil {
                log.Fatal(err)
        }
        defer rows.Close()
        td := trafficData{}
	data := trafficDataEach{}
	print := []trafficDataEach{}
	for rows.Next(){
                err := rows.Scan(&td.Street_id, &td.Street_length, &td.Vehicle_volume_1, &td.Avg_speed_1, &td.Vehicle_volume_2, &td.Avg_speed_2, &td.Vehicle_volume_3, &td.Avg_speed_3)
                if err!=nil{
                        log.Fatal(err)
		}
		data.Street_id = td.Street_id
		data.Vehicle_volume = td.Vehicle_volume_1
		data.Avg_speed = td.Avg_speed_3
		data.Street_length = td.Street_length
                print = append(print,data)
	}
	json.NewEncoder(w).Encode(&print)
        err = rows.Err()
}	

// Mendapatkan data dari Traffic Database setelah melakukan pemilihan 
// kolom volume kendaraan dan kecepatan rerata dengan meenggunakan fungsi random
func getRandomTrafficData(w http.ResponseWriter, r *http.Request) []infoTraffic {
	db,err := sql.Open("mysql", "root:@tcp(167.205.67.251:3306)/batifo_db")
        if err!=nil{
                log.Fatal(err)
        }
        defer db.Close()

	rdmvol := 1
	rdmavg := random(2,3)
	selectquery := "select street_id, street_length, vehicle_volume_" + strconv.Itoa(rdmvol) +" ,avg_speed_" + strconv.Itoa(rdmavg) + " from traffic_data"
        rows,err := db.Query(selectquery)
        if err!=nil {
                log.Fatal(err)
        }
        defer rows.Close()
	tde := trafficDataEach{}
	tarray := []trafficDataEach{}
	
	for rows.Next(){
                err := rows.Scan(&tde.Street_id, &tde.Street_length, &tde.Vehicle_volume, &tde.Avg_speed)
                if err!=nil{
                        log.Fatal(err)
		}
		data := tde
		tarray = append(tarray,data)
	}
	err = rows.Err()
	result := calculateClassification(tarray)
        return result
}
// Melakukan random sebuah angka dari 1-7 untuk menentukan data dari kolom mana yang akan digunakan
// untuk mendapatkan data Street
func getRandomStreetData(w http.ResponseWriter, r *http.Request)[]streetDataEach {
	db,err := sql.Open("mysql", "root:@tcp(167.205.67.251:3306)/batifo_db")
        if err!=nil{
                log.Fatal(err)
        }
        defer db.Close()

	rdmtime := random(1,7)
	selectquery := "select street_id, street_name, street_lat, street_lng, street_time" + strconv.Itoa(rdmtime) +" from street_data"
        rows,err := db.Query(selectquery)
        if err!=nil {
                log.Fatal(err)
        }
        defer rows.Close()
	sde := streetDataEach{}
	sarray := []streetDataEach{}
	
	for rows.Next(){
                err := rows.Scan(&sde.Street_id, &sde.Street_name, &sde.Street_lat, &sde.Street_lng, &sde.Street_time)
                if err!=nil{
                        log.Fatal(err)
		}
		data := sde
		sarray = append(sarray,data)
		
	}
	err = rows.Err()
	rs := getCurrentStreet(sarray)
	return rs
}