package main

import (
	"fmt"
	"log"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
)

func main(){
	port := 8080
	http.HandleFunc("/batifo/",func(w http.ResponseWriter, r *http.Request){
		http.ServeFile(w,r,"batifo.html")
	})
	http.HandleFunc("/jquery-3.2.1.min.js",func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w,r,"jquery-3.2.1.min.js")
	})
	http.HandleFunc("/street/",func(w http.ResponseWriter, r*http.Request){
	switch r.Method{
	case "GET" :
		w.Header().Add("Content-Type", "application/json")
		GetAllStreet(w,r)
	case "DELETE":

	default:
		http.Error(w, "Invalid request method",405)
	}
	})

	http.HandleFunc("/traffic/",func(w http.ResponseWriter, r*http.Request){
        switch r.Method{
	case "GET" :
		w.Header().Add("Content-Type", "application/json")
		GetAllTraffic(w,r)
	case "DELETE":
        default:
                http.Error(w, "Invalid request method",405)
        }
	})
	
	http.HandleFunc("/result/",func(w http.ResponseWriter, r*http.Request){
	switch r.Method{
	case "GET" :
		w.Header().Add("Content-Type", "application/json")
		getResult(w,r)
	case "DELETE":
	default:
		http.Error(w, "Invalid request method",405)
	}
	})

	log.Printf("Server starting on port %v\n",port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v",port),nil))

}


