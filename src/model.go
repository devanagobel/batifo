package main

type trafficData struct{
	Street_id string
	Street_length int
	Vehicle_volume_1 int
	Avg_speed_1 int
	Vehicle_volume_2 int
	Avg_speed_2 int
	Vehicle_volume_3 int
	Avg_speed_3 int
}

type trafficDataEach struct{
	Street_id string
	Street_length int
	Vehicle_volume int
	Avg_speed int	
}

type infoTraffic struct{
	Street_id string
	Street_status string
}

type streetTraffic struct{
	Street_name string
	Street_lat string
	Street_lng string
	Street_status string
}
type streetData struct {
	Street_id string
	Street_name string
	Street_lat string
	Street_lng string
	Street_time1 string
	Street_time2 string
	Street_time3 string
	Street_time4 string
	Street_time5 string
	Street_time6 string
	Street_time7 string
}
type streetDataEach struct{
	Street_id string
	Street_name string 
	Street_lat string
	Street_lng string
	Street_time string
}

type showStreet struct {
	Street_name string 
	Street_lat string
	Street_lng string
}
