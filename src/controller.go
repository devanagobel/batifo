package main

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Mengembalikan hasil acak angka untuk memilih kolom dari databasae dummy yang akan digunakan
func random(min, max int) int{
	rdm := rand.Intn(max-min) + min
	return (rdm)
}

// Fungsi untuk melakukan kalkulasi klasifikasi kemacetan berdasarkan panjang jalan, volume kendaraan, 
// dan kecepatan rata-rata kendaraan sesuai dengan metode perhitungan yang ditetapkan di spesifikasi 
func calculateClassification (tarray []trafficDataEach) []infoTraffic {
	var c float32
	data := infoTraffic{}
	result :=[]infoTraffic{}

	for _,element := range tarray{
		data.Street_id = element.Street_id
		length := element.Street_length
		volume := element.Vehicle_volume
		speed := element.Avg_speed

		c = (float32(length) * 3.5) / (float32(volume) * 4.5)
		if c > 1 {
			data.Street_status = "Lancar"
		} else if (c >= 0.5) && (c <= 1) {
			if (speed >= 40) {
				data.Street_status = "Lancar"
			} else if (speed >= 10) && (speed < 40){
				data.Street_status = "Lambat"
			}
		} else if (c < 0.5) {
			if (speed >= 10) && (speed < 40){
				data.Street_status = "Lambat"
			} else if (speed < 10){
				data.Street_status = "Macet"
			}
		}else{
			data.Street_status = "Not Defined"
		}
		result = append(result,data)
	}
	return result
}

// Mengembalikan data jalan sesuai dengan waktu pengguna mengakses web service
// Range waktu CurrentTime - 2 <= x <= CurrentTime + 2
// Dimana x merupakan waktu yang didapat dari database dummy Street_Data
func getCurrentStreet(sarray []streetDataEach)[]streetDataEach {
	today := time.Now()
	year,month,date := today.Date()

	max := time.Now().Unix() + (2 * 60 *60)
	min := time.Now().Unix() - (2 *60 *60)
	
	result := []streetDataEach{}
	
	for _,element := range sarray{
		stime := strings.Split(element.Street_time,":")
		shh,_ := strconv.Atoi(stime[0])
		smm,_ := strconv.Atoi(stime[1])
		
		ss1 := strings.Split(stime[2],".")
		sss,_ := strconv.Atoi(ss1[0])
		st := time.Date(year,month,date,shh,smm,sss,0,time.Local).Unix()
		if(st >= min && st <= max ){
				result 	= append (result,element)
		}
	}
	return result
}

// Mengembalikan hasil dari penggabungan data street dan traffic 
func mergeData (street []streetDataEach, traffic []infoTraffic) []streetTraffic {
	data := streetTraffic{}
	result := []streetTraffic{}
	for _,sd := range street{
		for _,td := range traffic{
			if(sd.Street_id == td.Street_id){
				data.Street_name = sd.Street_name
				data.Street_lat = sd.Street_lat
				data.Street_lng = sd.Street_lng
				data.Street_status = td.Street_status

				result = append(result,data)
			}
		}
	}
	return result
}
func getResult (w http.ResponseWriter, r *http.Request){
	street := getRandomStreetData(w,r)
	traffic := getRandomTrafficData(w,r)
	rslt := mergeData(street,traffic)
	json.NewEncoder(w).Encode(rslt)
}