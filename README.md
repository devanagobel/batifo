#BATIFO - Bandung Traffic Information#

BATIFO is a web service project for traffic information across streets in Bandung. Data are collected through dummy resource. 
BATIFO could be access via web browser or REST requests to the server. 

#FUNCTIONAL REQUIREMENTS#
1. Sistem dapat menyimpan data jalanan yang ada di Bandung
	1.1 Sistem memiliki 2 database yaitu StreetDB dan TrafficDB
	1.2 Data yang dimiliki oleh StreetDB adalah ID jalan, nama jalan, latitude jalan, dan longitude jalan, jam
	1.3 Data yang dimiliki oleh TrafficDB adalah ID jalan, panjang jalan, volume kendaraan, kecepatan rata-rata kendaraan
	1.4 Data yang didapat sudah dibuat sebelumnya di excel dan disimpan dalam format csv yang kemudian akan diimport kedalam database
2. Sistem dapat mengelompokan kateogori kemacetan di jalanan yang ada di Bandung
	2.1 Sistem dapat menghitung tingkat kemacetan di jalan tersebut menggunakan data lebar jalan, volume kendaraan, dan kecepatan rata-rata kendaraan
	2.2 Terdapat 3 kategori kemacetan yang dapat ditampilkan sistem, yaitu, Lancar, Lambat,dan Macet
3. Sistem dapat menampilkan list kondisi jalan saat web-service diakses
	3.1 Sistem dapat menerima request GET yang memberikan jam saat server diakses, kemudian sistem akan mencocokan jam server dengan jam yang terdapat dalam database dummy.
	3.2 Sistem hanya menampilkan data jalanan yang waktunya berada dalam range server diakses, yaitu diantara 2 jam sebelum server diakses dan 2 jam setelah server diakses.
4. Sistem tidak dapat menerima request POST dan DELETE karena diasumsikan seluruh data sudah tersedia di database dan sistem hanya dapat memberikan informasi kepada pengguna dan untuk menjaga keamanan data
5. Sistem dapat berjalan di OS Linux baik dengan/tanpa GUI.

#NON-FUNCTIONAL REQUIREMENTS#
1. Sistem memiliki database dan space yang cukup untuk menyimpan data selama 1 minggu
2. Sistem memiliki tampilan yang user-friendly bagi pengguna

#HARDWARE & SOFTWARE REQUIREMENTS#
1. Server
	1.1 Sistem diibuat menggunakan bahasa pemrograman GO Lang
	1.2 Sistem memiliki prosesor minimal 2GHz
	1.3 Sistem memiliki RAM minimal 2GB
	1.4 Sistem dikembangkan menggunakan OS Linux tanpa GUI
	
2. Client
	2.1 Sistem dapat diakses menggunakan web browser
	2.2 Client dapat mengakses menggunakan mobile device selama mobile device memiliki web browser



